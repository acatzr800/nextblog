import Head from 'next/head'
import Layout from '../components/layout'
import utilStyles from '../styles/utils.module.css'
import Link from "next/link";

export default function Favs() {
  return (
    <Layout favs>
      <Head>
        <title>Favorites</title>
      </Head>
      <section className={utilStyles.headingMd}>
        <p>Given the list located at <a href="https://gist.github.com/Pieparker/b04a4e9ff82ba949e5db9d5b0e9d89e8">this place over here.. [Github]</a></p>
      </section>
      <section className={utilStyles.headingMd}>
        <h2>Favorites:</h2>
        <ul>
          <li>Work with the product team to develop a new feature based on feedback from customers<br /><em>&mdash; I'm a big ideas kind of guy, so this is right up my alley.</em></li>
          <li>Act as a dedicated CSE for a handful of key customers to ensure their success using Vercel<br /><em>&mdash; I enjoy developing relationships and familiarity breeds trust.</em></li>
          <li>Help train and onboard new support teammates<br /><em>&mdash; I enjoy knowledge sharing.</em></li>
          <li>Identify, file (and, where possible, resolve) bugs in private and public Vercel/Next.js repos on GitHub<br/><em>&mdash; I love a good bug-squashing session.</em></li>
          <li>Dig through logs to troubleshoot a customer's broken project<br/><em>&mdash; More bug-squashing? I'm in.</em></li>
        </ul>
      </section>
      <section className={utilStyles.headingMd}>
        <h2>Least Favorites:</h2>
        <p>Nothing strikes me as particularly offensive, so I am listing based on personal preference or comfort.</p>
        <ul>
          <li>Create video tutorials to help teach users a specific feature or use case<br /><em>&mdash; I really just don't like the sound of my own voice.</em></li>
          <li>Respond to queries on Twitter, Reddit, Hacker News and other 3rd party sites<br /><em>&mdash; I generally try to avoid social media or comment sections.</em></li>
          <li>Work with 3rd party partners to track down a tricky situation for a joint customer<br/><em>&mdash; As a new employee, this would seem daunting.</em></li>
          <li>Analyze hundreds of support tickets to spot trends the product team can use<br /><em>&mdash; This might be for some people, but I feel like I would spend more time trying to automate this process.</em></li>
          <li>Engage multiple users at once in a public discussion, to answer their questions and troubleshoot problems<br/><em>&mdash; I prefer one-on-one interactions when it comes to identifying and fixing problems. The less noise and fewer cooks in the kitchen, the more reliably the problem can be fixed in the shortest amount of time.</em></li>
        </ul>
      </section>
    </Layout>
  )
}