import Head from 'next/head'
import Layout from '../components/layout'
import utilStyles from '../styles/utils.module.css'
import Link from "next/link";

export default function Problems() {
  return (
    <Layout problems>
      <Head>
        <title>What Are Some Common Problems?</title>
      </Head>
      <section className={utilStyles.headingMd}>
        <h2>What Are Some Common Problems?</h2>
        <p>Having used Vercel for about a week now, there haven't been any problems with the product itself. I am mainly still learning Next.js, so the bulk of my issues have been with code syntax and conventions.</p>
        <p>With Vercel itself, I could imagine many of the questions posed are regarding account and project scaling&mdash; migrating from a Hobby account to a Pro/Enterprise account, integrating with external services, controlling access with team members, 2FA/MFA tokens.</p>
        <p>From a technical side, I could see things like changed or expired tokens and authorizations (e.g. from GitHub or GitLab) being an issue, as well as configuring third-party domain names or setting up DNS services within Vercel.</p>
        <p>The best short term solutions are to provide cursory information on how to fix the problem but highlighting resources such as guides or documentation. Training customers and users to do a quick search for an answer before contacting support will help them become more self-sufficient in the long run while also reinforcing the brand image in their mind.</p>
        <p>To me, the mark of poor support is to have to rely on a human to find information for me. Good support is when I have the answers in just a few keystrokes or clicks. With that being said, many people prefer the opposite&mdash; someone to hold their hand and guide them through a personable experience. This, however, consumes team resources and must be balanced.</p>
      </section>
    </Layout>
  )
}