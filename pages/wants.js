import Head from 'next/head'
import Layout from '../components/layout'
import utilStyles from '../styles/utils.module.css'
import Link from "next/link";

export default function Wants() {
  return (
    <Layout wants>
      <Head>
        <title>What I want</title>
      </Head>
      <section className={utilStyles.headingMd}>
        <h2>What I Want?</h2>
        <p>I am a fairly easy-going kind of person and strive to be flexible in any given circumstance. I enjoy building a team up towards success. I like to learn new things and explore. I like to problem solve and approach issues from a novel perspective. I like to troubleshoot flaws and right sinking ships. I want to explore the barriers between dev, ops, and infrastructure. I have enjoyed learning Next.js and look forward to delving deeper.</p>
      </section>
    </Layout>
  )
}