import Head from 'next/head'
import Layout from '../components/layout'
import utilStyles from '../styles/utils.module.css'
import Link from "next/link";

export default function Redirect() {
  return (
    <Layout redirect>
      <Head>
        <title>Redirection</title>
      </Head>
      <section className={utilStyles.headingMd}>
        <h2>Redirection</h2>
        <p>Hello, thank you for contacting support. My name is Dan Campbell and I would be glad to help.</p>
        <p>If I understand correctly, it looks like you are having trouble setting up an in-app redirect to an external site. Please let me know if that is inaccurate. I would be happy to help!</p>
        <p>First, we want to edit the <code>`next.config.js`</code> file in your project's root directory. Create the file if it does not exist. Next, add the following code to the file:</p>
        <pre>
          <code dangerouslySetInnerHTML={htmlmarkup()} />
        </pre>
        <p>Change "/srcpath" to match the source of the redirect and "https://destination.com" to match the destination URL. If the redirect is not permanent, change <code>permanent: true</code> to <code>false</code>.</p>
        <p>Please verify that the redirect is now working.</p>
        <p>For more information, please visit: <a href="https://vercel.com/support/articles/does-vercel-support-permanent-redirects?query=redire#in-application-redirects">https://vercel.com/support/articles/does-vercel-support-permanent-redirects?query=redire#in-application-redirects</a>.</p>
        <p>For full documentation, please visit: <a href="https://nextjs.org/docs/api-reference/next.config.js/redirects">https://nextjs.org/docs/api-reference/next.config.js/redirects</a>.</p>
      </section>
    </Layout>
  )
}

export function htmlmarkup () {
  return {__html: 'module.exports = {\n  async redirects() {\n    return [\n      {\n        source: \'/srcpath\",\n        destination: \'https://destination.com\',\n        permanent: true,\n      },\n    ]\n  },\n}'};
}