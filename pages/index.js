import Head from "next/head";
import Layout, { siteTitle } from "../components/layout";
import utilStyles from "../styles/utils.module.css";
import { getSortedPostsData } from "../lib/posts";
import Link from "next/link";
import Date from "../components/date";

export async function getStaticProps() {
  const allPostsData = getSortedPostsData();
  return {
    props: {
      allPostsData,
    },
  };
}

export default function Home({ allPostsData }) {
  return (
    <Layout home>
      <Head>
        <title>{siteTitle}</title>
      </Head>
      <section className={utilStyles.headingMd}>
        <p>
          My name is Dan and I spend most of my life tethered to a computer or a
          phone. I enjoy technology, development, and facilitating others'
          attempts at world domination through my expertise.
        </p>
        <p>
          When I feel the need to unplug, I spend time in my woodworking shop
          making piles of sawdust and "happy accidents" that fuel my next
          campfire.
        </p>
        <h2>Assignments</h2>
        <ul>
          <li><Link href="/favs"><a>Favorites</a></Link></li>
          <li><Link href="/wants"><a>Wants</a></Link></li>
          <li><Link href="/influences"><a>Influences</a></Link></li>
          <li><a href="/hello-vercel">The Grand Redirect</a></li>
          <li><Link href="/redirect"><a>How to redirect?</a></Link></li>
          <li><Link href="/domains"><a>Custom domains</a></Link></li>
          <li><Link href="/problems"><a>Common problems</a></Link></li>
          <li><Link href="/support"><a>Improving This Exercise</a></Link></li>
        </ul>
      </section>
      <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
        <h2 className={utilStyles.headingLg}>Blog</h2>
        <ul className={utilStyles.list}>
          {allPostsData.map(({ id, date, title }) => (
            <li className={utilStyles.listItem} key={id}>
              <Link href={`/posts/${id}`}>
                <a>{title}</a>
              </Link>
              <br />
              <small className={utilStyles.lightText}>
                <Date dateString={date} />
              </small>
            </li>
          ))}
        </ul>
      </section>
    </Layout>
  );
}
