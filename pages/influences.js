import Head from 'next/head'
import Layout from '../components/layout'
import utilStyles from '../styles/utils.module.css'
import Link from "next/link";

export default function Influence() {
  return (
    <Layout wants>
      <Head>
        <title>Influences</title>
      </Head>
      <section className={utilStyles.headingMd}>
        <h2>Influences</h2>
        <p>I have been reading technical articles, watching TED(x) Talks, and consuming other bits of information for a very long time. Much longer than I wish to admit.</p>
        <p>I like to consume random scientific information, so I am subscribed to Channels such as <a href="https://www.youtube.com/c/smartereveryday">Smarter Every Day</a>, <a href="https://www.youtube.com/c/veritasium">Veritasium</a>, <a href="https://www.youtube.com/c/MarkRober">Mark Rober</a>, <a href="https://www.youtube.com/c/NileRed">NileRed</a>.. the list goes on. Most address fairly high-level topics, but are explained in a way that my seven-year-old enjoys and can understand.</p>
        <p>One of the people that stands out to me as being an incredibly entertaining and engaging person is Andrew Szydlo. He is routinely featured on our family room TV and one of his best presentations is available <a href="https://www.youtube.com/watch?v=bOuEJf8Dr_4">here on YouTube.</a></p>
        <p>Some of the most impactful articles that I have read are the series of Ubuntu server setup guides available on DigitalOcean's website. I say impactful, because not only are they a helpful sanity check when setting up servers, but because I know the writer, Brian Boucheron. We were colleagues for about eight years. His <a href="https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-20-04">tutorial on Ubuntu 20.04 setup</a> is still something I reference.</p>
      </section>
    </Layout>
  )
}