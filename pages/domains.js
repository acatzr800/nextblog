import Head from 'next/head'
import Layout from '../components/layout'
import utilStyles from '../styles/utils.module.css'
import Link from "next/link";

export default function Domains() {
  return (
    <Layout domains>
      <Head>
        <title>Adding a Custom Domain</title>
      </Head>
      <section className={utilStyles.headingMd}>
        <h2>Adding a Custom Domain</h2>
        <p>Hello, thank you for contacting support. My name is Dan Campbell and I would be glad to help.</p>
        <p>If I understand correctly, it looks like you would like to use a custom domain for your project. Please let me know if that is inaccurate. I would be happy to help!</p>
        <p>Please navigation to your Vercel dashboard and select your project. Next, click the Domains section under the Settings tab. Enter in the URL of the domain you wish to use, click Add, and follow the prompts. Once the domain has been added, you will need to verify your domain and change your domain's DNS settings on GoDaddy.</p>
        <p>For more information, please visit: <a href="https://vercel.com/support/articles/how-do-i-add-a-custom-domain-to-my-vercel-project">https://vercel.com/support/articles/how-do-i-add-a-custom-domain-to-my-vercel-project</a></p>
        <p>To change DNS settings on GoDaddy: <a href="https://www.godaddy.com/help/manage-dns-records-680">https://www.godaddy.com/help/manage-dns-records-680</a></p>
        <p>For full documentation, please visit: <a href="https://vercel.com/docs/concepts/projects/custom-domains">https://vercel.com/docs/concepts/projects/custom-domains</a></p>
        
      </section>
    </Layout>
  )
}