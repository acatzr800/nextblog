import Head from 'next/head'
import Layout from '../components/layout'
import utilStyles from '../styles/utils.module.css'
import Link from "next/link";

export default function Support() {
  return (
    <Layout support>
      <Head>
        <title>Improving This Exercise</title>
      </Head>
      <section className={utilStyles.headingMd}>
        <h2>Improving This Exercise</h2>
        <p>I don't have anything negative to say about this exercise. Resources were linked-to for those willing to notice them. I already had GitLab and Vercel accounts set up and projects running, so I didn't have any lag time to start up.</p>
      </section>
    </Layout>
  )
}